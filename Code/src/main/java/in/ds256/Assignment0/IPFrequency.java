package in.ds256.Assignment0;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
/**
 * DS-256 Assignment 0
 * Code for question 1(Frequency distribution for the number of URLs that are hosted on a single IP address) goes here
 */
public class IPFrequency 
{
    public static void main( String[] args )
    {
	SparkConf conf = new SparkConf().setAppName("IPFrequency");
        JavaSparkContext sc = new JavaSparkContext(conf);
        String path = args[0];

	/**
	 * Code goes here
	 */

        sc.stop();
    }
}
